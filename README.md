# D3.js - Basic Example
## Find example, and copy it
Here is a good example: http://bl.ocks.org/nbremer/21746a9668ffdf6d8242

All the source codes are here. So let's steal it.

## Create a new project
* Let's create a new folder, with 2 files (index.html, radarChart.js)
* Copy the code from the site above. 
* Open *index.html*

Fancy, huh?

## Check out our specification
Ok, so we need 3 separate chart. So let's triplicate this.

Find this line in *index.html*:
```html
<div class="radarChart"></div>
```
This is the div, which contains our spider chart. So let's do more.
```html
<div class="radarChart1 radarChart"></div>
<div class="radarChart2 radarChart"></div>
<div class="radarChart3 radarChart"></div>
```
Indexing the classes is not the best practice, but it's ok for now.

We have to edit our `<script>` too to draw something into these new divs.
Let's edit this line:
```js
//Call function to draw the Radar chart
RadarChart(".radarChart", data, radarChartOptions);
```
To this:
```js
//Call function to draw the Radar chart
RadarChart(".radarChart1", [data[0]], radarChartOptions);
RadarChart(".radarChart2", [data[1]], radarChartOptions);
RadarChart(".radarChart3", [data[2]], radarChartOptions);
```
And we also need to do a little styling to display these chart in one line. 
Add this CSS class to `<style>` part of the *index.html* file.
```css
.radarChart {
    display: inline-block;
}
```
And make them a little smaller:
Change this line
```js
var margin = {top: 100, right: 100, bottom: 100, left: 100},
    width = Math.min(700, window.innerWidth - 10) - margin.left - margin.right,
    height = Math.min(width, window.innerHeight - margin.top - margin.bottom - 20);
```
To this:
```js
var margin = { top: 100, right: 100, bottom: 100, left: 100 },
    width = 250,
    height = 250;
```
Nothing fancy calculating, just make them 250x250. 

**Check out, how does it look like!**

## Do the fancy thing
Everything in place so far. So let's do what Ivett wanted to do :D

**Disk background**
Let's add some background to these charts. 

After fast Google-ing, I found this solution:
Add these lines to the 'Glow filter for some extra pizzazz' part of *radarChart.js*:
```js
// Define Background
var defs= svg.append('defs')
defs.append('pattern')
    .attr('id', 'diskbg')
    .attr('patternUnits', 'objectBoundingBox')
    .attr('width', 1)
    .attr('height', 1)
    .append('svg:image')
    .attr('xlink:href', './img/disk.png')
    .attr("width", cfg.w)
    .attr("height", cfg.h)
    .attr("x", 0)
    .attr("y", 0);
```
And modify 'Draw background circles' part of the file:
```js
//Draw the background circles
axisGrid.selectAll(".levels")
    .data(d3.range(1, (cfg.levels + 1)).reverse())
    .enter()
    .append("circle")
    .attr("class", "gridCircle")
    .attr("r", function (d, i) { return radius / cfg.levels * d; })
    .style("fill", function(d,i) {
        if (i == 0) {
            return "url(#diskbg)"
        }
        return "none";
    })
    .style("stroke", "#CDCDCD")
    .style("fill-opacity", cfg.opacityCircles);
```
All we did here is to change the fill property, and remove the filter. 
We defined a function to decide weather is a background or not to the given circle. That's because, we only need to draw background to the first circle. All the others will be transparent. 

One last thing. We have to increase the value of oppacity in cfg variable:
```js
opacityCircles: 0.7,
```

**Gradient overlay**
We have to define gradient like we did with the background:
```js
// Define radial gradient
var grads = svg.append("defs").selectAll("radialGradient").data(data)
    .enter().append("radialGradient")
    .attr("gradientUnits", "userSpaceOnUse")
    .attr("cx", 0)
    .attr("cy", 0)
    .attr("r", "100%")
    .attr("id", "grad");
grads.append("stop").attr("offset", "5%").style("stop-color", "red");
grads.append("stop").attr("offset", "10%").style("stop-color", "yellow");
grads.append("stop").attr("offset", "20%").style("stop-color", "green");
```
And pass this def to the fill of our radarArea element. Find these line, and modified to this:
```js
//Append the backgrounds	
blobWrapper
    .append("path")
    .attr("class", "radarArea")
    .attr("d", function (d, i) { return radarLine(d); })
    .style("fill", "url(#grad)")
    .style("fill-opacity", cfg.opacityArea)
    .on('mouseover', function (d, i) {
        //Dim all blobs
        d3.selectAll(".radarArea")
            .transition().duration(200)
            .style("fill-opacity", 0.1);
        //Bring back the hovered over blob
        d3.select(this)
            .transition().duration(200)
            .style("fill-opacity", 0.7);
    })
    .on('mouseout', function () {
        //Bring back all blobs
        d3.selectAll(".radarArea")
            .transition().duration(200)
            .style("fill-opacity", cfg.opacityArea);
    });
```

## Simulate data
This chart is so static at this moment. Let's make a little more dynamic.

Create this function to the end of `<script>` tag in *index.html*:
```js
function randomData() {
    return [
        { axis: "Battery Life", value: Math.random() },
        { axis: "Brand", value: Math.random() },
        { axis: "Contract Cost", value: Math.random() },
        { axis: "Design And Quality", value: Math.random() },
        { axis: "Have Internet Connectivity", value: Math.random() },
        { axis: "Large Screen", value: Math.random() },
        { axis: "Price Of Device", value: Math.random() },
        { axis: "To Be A Smartphone", value: Math.random() }
    ]
}
```
And modify the chart definitions to this:
```js
//Call function to draw the Radar chart
RadarChart(".radarChart1", [randomData()], radarChartOptions);
RadarChart(".radarChart2", [randomData()], radarChartOptions);
RadarChart(".radarChart3", [randomData()], radarChartOptions);
```

And define max value. Change this line in *radarChart.js*:
```js
// var maxValue = Math.max(cfg.maxValue, d3.max(data, function (i) { return d3.max(i.map(function (o) { return o.value; })) }));
```
To this:
```js
var maxValue = 1;
```
We are simple here like an Akos song. :P
